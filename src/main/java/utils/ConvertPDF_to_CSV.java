package utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Mateus Oliveira
 */
public class ConvertPDF_to_CSV {

    public static void Convert_Pdf_To_CSV() throws IOException {

        Workbook wb; //worbook para trabalhar com apache poi, biblioteca para manipular excel files (xls, xlsx..)
        XSSFSheet sheet; //"sheet = aba", objeto "aba" onde vou manipular linhas,colunas,etc dos arquivos a ser lido e escrito 
        Row row;//objeto para manipulação de linhas
        wb = new XSSFWorkbook();
        sheet = (XSSFSheet) wb.createSheet("Excel");
        Cell cell;
        XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();
        Integer a;
        
        XSSFFont font = (XSSFFont) wb.createFont();
        font.setBold(true);
        style.setFont(font);
        PDDocument document = PDDocument.load(new File("exemplopdf.pdf"));
        document.getClass();
        if (!document.isEncrypted()) {

            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition(true);

            PDFTextStripper tStripper = new PDFTextStripper();

            String pdfFileInText = tStripper.getText(document);
            //System.out.println("Text:" + st);

            // split by whitespace
            String lines[] = pdfFileInText.split("\\r?\\n");
            int i = 0;
            int j;
            String[] split;
            int flag = 0;
            int pulaPrimeirasLinhas = 0;
            String salvaPrimeiraLinha = "";
            for (String line : lines) {
                j = 0;
                if (pulaPrimeirasLinhas == 0 || pulaPrimeirasLinhas == 1) {
                    if (pulaPrimeirasLinhas == 0) {
                        salvaPrimeiraLinha = line;
                    }
                    pulaPrimeirasLinhas++;
                    i = 1;
                    continue;

                }
                System.out.println(line);
                row = sheet.createRow(i);
                cell = row.createCell(j);
                cell.setCellValue(line);

                if (line.contains("RESUMO")) {
                    flag = 1;
                    cell.setCellStyle(style);
                    i++;
                    continue;

                } else if (line.contains("CONTA: ")) {
                    split = line.split(":|/");

                    for (String s : split) {
                        cell = row.createCell(j);
                        cell.setCellValue(s);
                        if (s.contains("CONTA")) {
                            cell.setCellStyle(style);
                        }
                        j++;
                    }
                    i++;
                } else if (line.contains("COOP.:")) {
                    split = line.split(":|/");

                    for (String s : split) {
                        cell = row.createCell(j);
                        cell.setCellValue(s);
                        if (s.contains("COOP")) {
                            cell.setCellStyle(style);
                        }
                        j++;
                    }

                } else if (line.contains("DATA")) {
                    split = line.split("\\s+");
                    for (String s : split) {
                        cell = row.createCell(j);
                        cell.setCellValue(s);
                        cell.setCellStyle(style);
                        j++;
                    }

                } else if (line.contains("VALOR")) {
                    cell.setCellStyle(style);
                } else if (line.contains("DOCUMENTO")) {
                    cell.setCellStyle(style);
                } else if (line.contains("EXTRATO CONTA")) {
                    cell.setCellStyle(style);
                } else if (line.contains("OUVIDORIA SICOOB")) {
                    cell.setCellStyle(style);
                } else if (line.contains("000")) {
                    cell.setCellStyle(style);
                }
                if (flag == 1) {
                    split = line.split(": ");
                    if (split.length >= 2) {
                        for (String s : split) {
                            cell = row.createCell(j);
                            cell.setCellValue(s);
                            j += 3;
                        }
                    }
                    else if (split.length == 1) {
                        flag = 0;
                    }
                }

                i++;
            }
            row = sheet.createRow(0);
            cell = row.createCell(0);
            cell.setCellValue(salvaPrimeiraLinha);
            cell.setCellStyle(style);

        }
        FileOutputStream fileOut = new FileOutputStream("testeCsv.csv");

        wb.write(fileOut);

    }

}
